import tkinter as tk


class Course:
    def __init__(self, name, code, requisites, semester,nro_semestre, i,frame):
        self.name = name
        self.code = code
        if requisites == ['']:
            self.requisites = []
        else:
            self.requisites = requisites
        self.semester = semester
        self.button = tk.Button(frame, text=code+"\n"+name[0:12], command=self.callback)
        self.button.grid(column=i,row=nro_semestre)
        self.button["bg"]="gray"
        self.pressed = False
        self.malla = None

    def callback(self):
        pass

    def next_requisite(self):
        if self.pressed:
            self.pressed = False
            self.button["bg"]="gray"
            self.malla.remove_dependecy(self)
        else:
            self.pressed = True
            self.button["bg"]="green"
            self.malla.get_dependency(self)

    def get_code(self):
        return self.code

    def get_requisite_codes(self):
        return self.requisites

    def is_available_in(self, semester):
        return semester in self.semester

    def __str__(self):
        semestre = ""
        if "O" in self.semester:
            semestre = " Otono"
        if "P" in self.semester:
            semestre += " Primavera"
        return self.code + "-" + self.name + semestre

    def set_malla(self, malla):
        self.malla = malla

    def is_requisite(self, a_course):
        if a_course.get_code() in self.requisites:
            self.button["bg"]="red"

    def remove_dependency(self, a_course):
        if a_course.get_code() in self.requisites:
            self.button["bg"] = "gray"
