class Person:
    def __init__(self, name, courses, malla):
        self.name = name
        self.aprobed_courses = courses
        self.malla = malla

    def next(self, semester="O"):
        aprobed_codes = []
        for course in self.aprobed_courses:
            aprobed_codes.append(course.get_code())
        malla_courses = self.malla.get_courses()
        aprobed_prerequisites = []
        for course in malla_courses:
            if course.get_code() in aprobed_codes:
                continue
            prerequisite = True
            for requisite in course.get_requisite_codes():
                prerequisite = prerequisite and (requisite in aprobed_codes)
            if prerequisite:
                if course.is_available_in(semester):
                    aprobed_prerequisites.append(course)
        return aprobed_prerequisites

