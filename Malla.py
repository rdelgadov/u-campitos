class Malla:
    def __init__(self, courses):
        self.courses = courses
        for course in self.courses:
            course.set_malla(self)

    def get_codes(self):
        codes = []
        for course in self.courses:
            codes.append(course.get_code())
        return codes

    def get_courses(self):
        return self.courses

    def get_dependency(self, the_course):
        for course in self.courses:
            course.is_requisite(the_course)

    def remove_dependecy(self, a_course):
        for course in self.courses:
            course.remove_dependency(a_course)
