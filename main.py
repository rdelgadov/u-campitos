from Course import Course
from Malla import Malla
from Person import Person
import csv
import tkinter as tk
from util import setup


window = tk.Tk()
frame2 = tk.Frame(window)
frame2.pack()
frame = tk.Frame(frame2)
frame.grid(sticky=tk.E)
frameleft = tk.Frame(frame2)
frameleft.grid(sticky=tk.W)
button = tk.Button(frameleft, text="click me!")
button.pack()
malla = []
with open("malla_icc.csv") as csvfile:
    reader = csv.DictReader(csvfile, delimiter=",")
    i = 0
    nro_semestre = 0
    for row in reader:
        if nro_semestre != int(row['nro_semestre']):
            i = 0
            nro_semestre = int(row["nro_semestre"])
        malla.append(
            Course(row["nombre"], row["codigo"], row["requisitos"].split(";"), row["semestre"].split(";"), nro_semestre,
                   i, frame))
        i += 1
malla_icc = Malla(malla)

if __name__ == '__main__':
    # Si pedrito aprobo int matematicas discretas, Fisica 1, herramientas y quimica. Que ramos puede tomar el proximo semestre?
    aprobed = malla[0:11]
    pedrito = Person("Pedrito", aprobed, malla_icc)
    for course in aprobed:
        print(course)
    print("Proximos:")
    pn = pedrito.next("O")
    for course in pn:
        print(course)
    window.mainloop()
